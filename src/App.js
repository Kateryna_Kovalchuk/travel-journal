import './App.css';
import Header from './Components/Header';
import Card from './Components/Card';
import data from './data';

function App() {
    const cardElements = data.map(value => {
        return <Card key={value.id} {...value} />
    })

    return (
        <div>
            <Header />
            <main className='card-list'>{cardElements}</main>
        </div>
    );
}

export default App;

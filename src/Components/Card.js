
export default function Card(props){

    return(
        <div className='card'>
            <img className='card-image' src={`./images/${props.imageURL}`} alt={props.title} />
            <div className='card-content'>
                <div className='card-location'>
                    <img src='./images/location.jpg' className='card-marker' />
                    <span className='card-country'>{props.location}</span>
                    <a className='card-link' href={props.GoogleMapsLink} target='_blank'>View on Google Maps</a>
                </div>
                <h1 className='card-title'>{props.title}</h1>
                <b>{props.startDate} - {props.endDate}</b>
                <p>{props.description}</p>
            </div>
        </div>
    )
}
